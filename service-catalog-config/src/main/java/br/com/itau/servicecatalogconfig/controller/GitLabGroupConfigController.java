package br.com.itau.servicecatalogconfig.controller;

import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.servicecatalogconfig.model.GitLabGroupConfig;
import br.com.itau.servicecatalogconfig.repository.GitLabGroupConfigRepository;

@RestController
@RequestMapping("/config")
public class GitLabGroupConfigController {
	
		@Autowired
		GitLabGroupConfigRepository gitConfiRepo ;
		
	  private final Logger log = LoggerFactory.getLogger("GitLabGroupConfigController");
	  
	  @Value("${config.cronscheduler,default}")
		private String schedulerDefault;
	  
		
		@RequestMapping(method=RequestMethod.GET)
		public ResponseEntity<?> listGitConfig(){
			return ResponseEntity.status(200).body(gitConfiRepo.findAll());
		}
		
		@RequestMapping(path="/{id}", method=RequestMethod.GET)
		public ResponseEntity<?> getConfig(@PathVariable Long id){
			return ResponseEntity.status(200).body(gitConfiRepo.findById(id));
		}
		
		@RequestMapping(method=RequestMethod.POST)
		public ResponseEntity<?> postGitLabConfi(@RequestBody GitLabGroupConfig gitLabGroupConfig){
			
			try {
				
				if(gitLabGroupConfig.getCronScheduler() == null || "".equals(gitLabGroupConfig.getCronScheduler())) {
					
				  log.info("Campo cronScheduler vazio, utilizando valor default: ");
				  gitLabGroupConfig.setCronScheduler(schedulerDefault);
				  
				  
				}
				
				
				gitLabGroupConfig.getGitLabToken().setDateCreated(Calendar.getInstance());
				gitLabGroupConfig.getGitLabToken().setDateExpire(Calendar.getInstance());

				return ResponseEntity.status(201).body(gitConfiRepo.save(gitLabGroupConfig));
			}
			catch (Exception e) {
				return ResponseEntity.badRequest().build();			
			}
		}
		

}
