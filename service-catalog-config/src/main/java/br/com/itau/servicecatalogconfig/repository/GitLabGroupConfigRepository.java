package br.com.itau.servicecatalogconfig.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.servicecatalogconfig.model.GitLabGroupConfig;;

public interface GitLabGroupConfigRepository extends CrudRepository<GitLabGroupConfig, Integer> {
	public GitLabGroupConfig findById(Long id);

}
